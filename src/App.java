import models.*;

public class App {
    public static void main(String[] args) throws Exception {
        System.out.println("Hello, World!");
        //  khởi tạo hai đối tượng 
        Account account1 = new Account("10226", "thuong");
        Account account2 = new Account("99999", "duong", 10000000);
        System.out.println(">>>>account1");
        System.out.println("id là : " + account1.getId());
        System.out.println("tên là : " + account1.getName());
        System.out.println("số dư ban đầu " + account1.getBalance());
        System.out.println(account1.getClass());
        System.out.println("số dư sau cộng tiền " + account1.credit(300000));
        System.out.println(account1.getBalance());
        System.out.println("số dư sau trừ  tiền : " + account1.debit(100000));
        System.out.println("số dư sau trừ tiền : " + account1.getBalance());
        // chuyển tiền đến tài khoản khác (account2)
        System.out.println(account1.transferTo(account2,50000));
        System.out.println("số dư sau chuyển tiền tiền : " + account1.getBalance());
        System.out.println(account1);

        System.out.println(">>>>account2");
        System.out.println("số dư ban đầu " + account2.getBalance());
        System.out.println(account2);
        // chuyển tiền từ tài khoản hai sang tài khoản 1 
        account2.transferTo(account1, 5000000);
        System.out.println(account1);
        System.out.println(account2);





    }
}
