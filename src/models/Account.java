package models;

public class Account {
    private String id; // id
    private String name; // tên
    private int balance = 0; // cân đối
    // khơi tạo nấu truyền vào hai tham số
    public Account(String id, String name) {
        this.id = id;
        this.name = name;
    }
    // khởi tạo nếu chuyền vào 3 tham số 
    public Account(String id, String name, int balance) {
        this.id = id;
        this.name = name;
        this.balance = balance;
    }

    public String getId() {
        return id;
    }

    public String getName() {
        return name;
    }

    public int getBalance() {
        return balance;
    }

    // thêm tiền vào số dư
    public int credit(int amount) {
        //  tăng số dư đang có thêm amount
        this.balance = balance + amount;
        return this.balance;
    }

    // ghi nợ  // trừ tiền đi 
    public int debit(int amount) {
        // nếu như số tiền amount nhỏ hơn số dư thì
        if (amount <= balance) {
            this.balance = balance - amount;
        } else {
            System.out.println("số tiền bạn nhập vượt quá số dư");
        }
        return this.balance;

    }

    // phương thức chuyển tiền
    // input 1 tài khoản khác Account, và số tiền amount
    // thực hiện cộng tiền vào tài khoản mới, trừ tiền vaò tài khoản đang chuyển
    // tiền
    // trả về số tiền còn lại của tài khoản cũ
    public int transferTo(Account another, int amount) {
        // nếu như số tiền amount nhỏ hơn số dư thì
        // cộng tiền vào tài khoản another

        if (amount <= balance) {
            // cộng tiền vào tài khoản mới
            another.credit(amount);
            // số tiền còn lại sau chuyển tiền
            this.debit(amount);
        } else {
            System.out.println("số tiền bạn nhập vượt quá số dư");
        }
        return this.balance;
    }

    @Override
    public String toString() {
        return "Account [id=" + id + ", name=" + name + ", balance=" + balance + "]";
    }

    

}
